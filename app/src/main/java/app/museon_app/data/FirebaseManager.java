package app.museon_app.data;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.HashMap;
import java.util.Map;

import app.museon_app.model.Artist;
import app.museon_app.model.CulturalAsset;

/**
 * Created by sapi9 on 05/08/2017.
 */

public class FirebaseManager {
    private Map<String, CulturalAsset> piecesOfArt = new HashMap<>();
    private Map<String,Artist> artists = new HashMap<>();
    public void setPiecesOfArtListener() {

        FirebaseDatabase.getInstance().getReference("artists").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Artist a = snap.getValue(Artist.class);
                    artists.put(a.getKey(),a);
                }

                FirebaseDatabase.getInstance().getReference("pieces-of-art").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        piecesOfArt.clear();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            CulturalAsset p = snap.getValue(CulturalAsset.class);
                            if(p.getArtist()!=null){
                                p.setMappedArtist(artists.get(p.getArtist()));
                            }

                            String formattedKey = p.getBeacon_uuid() != null && p.getBeacon_major() != null && p.getBeacon_minor() != null ?
                                    p.getBeacon_uuid().toUpperCase() + ":"
                                            + p.getBeacon_major().toUpperCase() + ":"
                                            + p.getBeacon_minor().toUpperCase()
                                    : null;
                            if (formattedKey != null) {
                                piecesOfArt.put(formattedKey, p);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public Map<String, CulturalAsset> getPiecesOfArt() {
        return piecesOfArt;
    }

}
