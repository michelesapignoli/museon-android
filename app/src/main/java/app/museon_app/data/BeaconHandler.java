package app.museon_app.data;

import org.altbeacon.beacon.Beacon;

/**
 * Created by sapi9 on 04/10/2017.
 */

public class BeaconHandler {
    private static final BeaconHandler ourInstance = new BeaconHandler();

    private Beacon currentBeacon;

    private BeaconHandler() {
    }


    public static BeaconHandler getInstance() {
        return ourInstance;
    }

    public void setCurrentBeacon(Beacon beacon){
        this.currentBeacon = beacon;
    }

    public Beacon getCurrentBeacon(){
        return this.currentBeacon;
    }
}
