package app.museon_app.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;

import app.museon_app.R;
import app.museon_app.model.Artist;

/**
 * Created by sapi9 on 12/10/2017.
 */

public class ArtistFragment extends AppCompatDialogFragment {
    private TextView name;
    private TextView born;
    private TextView city;
    private TextView dead;
    private TextView art_movement;
    private TextView description;
    private TextView name_label;
    private TextView born_label;
    private TextView city_label;
    private TextView dead_label;
    private TextView art_movement_label;
    private TextView description_label;
    private ImageView image;
    private Artist artist;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.artist = (Artist)getArguments().getSerializable("artist");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.dialog_artist, null);
        name = v.findViewById(R.id.dialog_name);
        born = v.findViewById(R.id.dialog_born);
        city = v.findViewById(R.id.dialog_city);
        dead = v.findViewById(R.id.dialog_dead);
        art_movement = v.findViewById(R.id.dialog_art_movement);
        description = v.findViewById(R.id.dialog_description);
        image = v.findViewById(R.id.dialog_image);
        name_label = v.findViewById(R.id.dialog_name_label);
        born_label = v.findViewById(R.id.dialog_born_label);
        city_label = v.findViewById(R.id.dialog_city_label);
        dead_label = v.findViewById(R.id.dialog_dead_label);
        art_movement_label = v.findViewById(R.id.dialog_art_movement_label);
        description_label = v.findViewById(R.id.dialog_description_label);
        this.populateDetail(artist);

        builder.setView(v);
        return builder.create();
    }

    void populateDetail(Artist detail) {
        if (detail != null) {
            Glide.with(this)
                    .using(new FirebaseImageLoader())
                    .load(FirebaseStorage.getInstance().getReference("images").child("artists").child(detail.getKey()+"."+detail.getExt()))
                    .into(this.image);
            this.name.setText(detail.getName());
            this.write(this.born, detail.getBorn(), this.born_label);
            this.write(this.city, detail.getCity(), this.city_label);
            this.write(this.dead, detail.getDead(), this.dead_label);
            this.write(this.art_movement, detail.getArt_movement(), this.art_movement_label);
            this.write(this.description, detail.getDescription(), this.description_label);
        }
    }


    private void write(TextView t, String what, TextView label) {
        if (what != null) {
            t.setText(what);
        } else {
            label.setVisibility(View.GONE);
            t.setVisibility(View.GONE);
        }
    }
}
