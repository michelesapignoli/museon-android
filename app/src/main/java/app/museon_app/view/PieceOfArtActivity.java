package app.museon_app.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;

import app.museon_app.R;
import app.museon_app.model.CulturalAsset;

/**
 * Created by sapi9 on 03/10/2017.
 */

public class PieceOfArtActivity extends MuseonActivity {
    private TextView author;
    private TextView date;
    private TextView technique;
    private TextView dimensions;
    private TextView art_movement;
    private TextView location;
    private TextView description;
    private TextView author_label;
    private TextView date_label;
    private TextView technique_label;
    private TextView dimensions_label;
    private TextView art_movement_label;
    private TextView location_label;
    private TextView description_label;
    private ImageView image;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        final CulturalAsset detail = (CulturalAsset) intent.getSerializableExtra("viewing");

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.toolbar_layout);
        fab = findViewById(R.id.fab);
        if(detail.getMappedArtist()!= null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatDialogFragment f = new ArtistFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("artist", detail.getMappedArtist());
                    f.setArguments(args);
                    f.show(getSupportFragmentManager(), "artistdialog");
                }
            });
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.author = findViewById(R.id.detail_author);
        this.date = findViewById(R.id.detail_date);
        this.technique = findViewById(R.id.detail_technique);
        this.dimensions = findViewById(R.id.detail_dimensions);
        this.art_movement = findViewById(R.id.detail_art_movement);
        this.location = findViewById(R.id.detail_location);
        this.description = findViewById(R.id.detail_description);
        this.author_label = findViewById(R.id.detail_author_label);
        this.date_label = findViewById(R.id.detail_date_label);
        this.technique_label = findViewById(R.id.detail_technique_label);
        this.dimensions_label = findViewById(R.id.detail_dimensions_label);
        this.art_movement_label = findViewById(R.id.detail_art_movement_label);
        this.location_label = findViewById(R.id.detail_location_label);
        this.description_label = findViewById(R.id.detail_description_label);
        this.image = findViewById(R.id.toolbar_image);


        this.populateDetail(detail, collapsingToolbar);

    }

    @Override
    void setContentView() {
        setContentView(R.layout.activity_piece_of_art);
    }

    void populateDetail(CulturalAsset detail, CollapsingToolbarLayout toolbar) {
        if (detail != null) {
            Glide.with(this)
                    .using(new FirebaseImageLoader())
                    .load(FirebaseStorage.getInstance().getReference("images").child("pieces").child(detail.getKey()+"."+detail.getExt()))
                    .into(this.image);
            if(detail.getMappedArtist() != null){
                Glide.with(this)
                        .using(new FirebaseImageLoader())
                        .load(FirebaseStorage.getInstance().getReference("images").child("artists").child(detail.getMappedArtist().getKey()+"."+detail.getMappedArtist().getExt()))
                        .into(this.fab);
            }

            toolbar.setTitle(detail.getTitle());
            this.author.setText(detail.getMappedArtist().getName());
            this.write(this.date, detail.getDate(), this.date_label);
            this.write(this.technique, detail.getTechnique(), this.technique_label);
            this.write(this.dimensions, detail.getDimensions(), this.dimensions_label);
            this.write(this.art_movement, detail.getArt_movement(), this.art_movement_label);
            this.write(this.location, detail.getLocation(), this.location_label);
            this.write(this.description, detail.getDescription(), this.description_label);
        }
    }

    private void write(TextView t, String what, TextView label) {
        if (what != null) {
            t.setText(what);
        } else {
            label.setVisibility(View.GONE);
            t.setVisibility(View.GONE);
        }
    }
}
