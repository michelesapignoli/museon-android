package app.museon_app.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by sapi9 on 03/10/2017.
 */

public abstract class MuseonActivity extends AppCompatActivity {
    protected View mContentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView();
    }

    abstract void setContentView();
}
