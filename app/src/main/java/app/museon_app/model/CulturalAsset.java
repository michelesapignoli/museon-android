package app.museon_app.model;

import java.io.Serializable;

/**
 * Created by sapi9 on 05/08/2017.
 */

public class CulturalAsset implements Serializable {
    private String key;
    private String title;
    private String artist;
    private Artist mappedArtist;
    private String date;
    private String technique;
    private String art_movement;
    private String description;
    private String imageStoragePath;
    private String ext;
    private String beacon_uuid;
    private String beacon_major;
    private String beacon_minor;
    private String dimensions;
    private String location;
    private String url;

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public Artist getMappedArtist() {
        return mappedArtist;
    }

    public String getDate() {
        return date;
    }

    public String getTechnique() {
        return technique;
    }

    public String getArt_movement() {
        return art_movement;
    }

    public String getDescription() {
        return description;
    }

    public String getImageStoragePath() {
        return imageStoragePath;
    }

    public String getExt() {
        return ext;
    }

    public String getBeacon_uuid() {
        return beacon_uuid;
    }

    public String getBeacon_major() {
        return beacon_major;
    }

    public String getBeacon_minor() {
        return beacon_minor;
    }
    public String getDimensions(){
        return dimensions;
    }
    public String getLocation(){
        return location;
    }
    public String getImageUrl(){
        return url;
    }
    public void setMappedArtist(Artist mappedArtist){
        this.mappedArtist = mappedArtist;
    }
}
